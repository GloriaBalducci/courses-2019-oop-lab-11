﻿using System;

namespace Cards
{
    class Deck
    {
        private Card[] cards;

        public Deck()
        {

        }

        public void Initialize()
        {
            cards = new Card[Enum.GetNames(typeof(ItalianValue)).Length * Enum.GetNames(typeof(ItalianSeed)).Length];
            /*
             * == README ==
             * 
             * I due enumerati (ItalianSeed e ItalianValue) definiti in fondo a questo file rappresentano rispettivamente semi e valori
             * di un tipo mazzo di carte da gioco italiano.
             * 
             * Questo metodo deve inizializzare il mazzo (l'array cards) considerando tutte le combinazioni possibili.
             * 
             * Nota - Dato un generico enumerato MyEnum:
             *   a) è possibile ottenere il numero dei valori presenti con Enum.GetNames(typeof(MyEnum)).Length
             *   b) è possibile ottenere l'elenco dei valori presenti con (MyEnum[])Enum.GetValues(typeof(MyEnum))
             * 
             */
            try
            {
                int count = 0;
                foreach (ItalianSeed seed in (ItalianSeed[])Enum.GetValues(typeof(ItalianSeed)))
                {
                    foreach (ItalianValue value in (ItalianValue[])Enum.GetValues(typeof(ItalianValue)))
                    {
                        cards[count] = new Card(seed.ToString(), value.ToString());
                        cards[count].Seed = seed.ToString();
                        cards[count].Value = value.ToString();
                        count++;
                    }
                }
            }
            catch
            {
                throw new NotImplementedException();
            }
        }

        public void Print()
        {
            /*
             * == README  ==
             * 
             * Questo metodo stampa tutte le carte presenti nel mazzo, con una propria rappresentazione a scelta.
             */
            try
            {
                Console.WriteLine("ci arrivo");
                foreach (Card i in cards)
                {
                    Console.WriteLine(i.ToString());
                }
            }
            catch
            {
                throw new NotImplementedException();
            }
        }


        
    }
        enum ItalianSeed
    {
        DENARI =1,
        COPPE =2,
        SPADE=3,
        BASTONI=4
    }

    enum ItalianValue
    {
        ASSO=1,
        DUE=2,
        TRE=3,
        QUATTRO=4,
        CINQUE=5,
        SEI=6,
        SETTE=7,
        FANTE=8,
        CAVALLO=9,
        RE=10
    }
    class FrenchDeck
    {
        private FrenchCard[] frenchcards;

        public FrenchDeck()
        {

        }

        public void Initialize()
        {
            frenchcards = new FrenchCard[Enum.GetNames(typeof(FrenchValue)).Length * Enum.GetNames(typeof(FrenchSeed)).Length+2];
            /*
             * == README ==
             * 
             * I due enumerati (ItalianSeed e ItalianValue) definiti in fondo a questo file rappresentano rispettivamente semi e valori
             * di un tipo mazzo di carte da gioco italiano.
             * 
             * Questo metodo deve inizializzare il mazzo (l'array cards) considerando tutte le combinazioni possibili.
             * 
             * Nota - Dato un generico enumerato MyEnum:
             *   a) è possibile ottenere il numero dei valori presenti con Enum.GetNames(typeof(MyEnum)).Length
             *   b) è possibile ottenere l'elenco dei valori presenti con (MyEnum[])Enum.GetValues(typeof(MyEnum))
             * 
             */
            try
            {
                int count = 0;
                foreach (FrenchSeed seed in (FrenchSeed[])Enum.GetValues(typeof(FrenchSeed)))
                {
                    foreach (FrenchValue value in (FrenchValue[])Enum.GetValues(typeof(FrenchValue)))
                    {
                        frenchcards[count] = new FrenchCard(seed.ToString(), value.ToString());
                        frenchcards[count].Seed = seed.ToString();
                        frenchcards[count].Value = value.ToString();
                        count++;
                    }

                }
                foreach(FrenchJolly jolly in (FrenchJolly[])Enum.GetValues(typeof(FrenchJolly)))
                {
                    foreach(FrenchJollyColore colore in (FrenchJollyColore[])Enum.GetValues(typeof(FrenchJollyColore)))
                    {
                        frenchcards[count] = new FrenchCard(jolly.ToString(), colore.ToString());
                        frenchcards[count].Seed = jolly.ToString();
                        frenchcards[count].Value = colore.ToString();
                        count++;
                    }
                }
            }
            catch
            {
                throw new NotImplementedException();
            }
        }

        public void Print()
        {
            /*
             * == README  ==
             * 
             * Questo metodo stampa tutte le carte presenti nel mazzo, con una propria rappresentazione a scelta.
             */
            try
            {
                Console.WriteLine("ci arrivo");
                foreach (FrenchCard i in frenchcards)
                {
                    Console.WriteLine(i.ToString());
                }
            }
            catch
            {
                throw new NotImplementedException();
            }
        }



    }
    enum FrenchSeed
    {
        CUORI = 1,
        QUADRI = 2,
        FIORI = 3,
        PICCHE = 4
    }

    enum FrenchValue
    {
        ASSO = 1,
        DUE = 2,
        TRE = 3,
        QUATTRO = 4,
        CINQUE = 5,
        SEI = 6,
        SETTE = 7,
        OTTO = 8,
        NOVE = 9,
        DIECI = 10,
        JACK=11,
        DONNA=12,
        RE=13
    }

    enum FrenchJolly
    {
        JOLLY=1
       
    }

    enum FrenchJollyColore
    {
        NERO=1,
        ROSSO=2
    }
}
